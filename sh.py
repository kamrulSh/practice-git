# Defining the function named hello
def hello():
    print("Hello World!")

# new feature afdded by kamrul bjit
# We can track commits from any email address not part of your Atlassian account. 
# To set this up, enter email addresses you'd like to use as aliases and confirm them. 
# You can't use email aliases to log in to Bitbucket or accept invitations.
# Calling the function to use it
hello()
hello()
# hello()
# hello()
# hello()
#  this line is duplicate

# Again calling the function
hello()
# new feature added
# hello() function
hello()
# hello()
# hello()
# hello()
#  this line is duplicate

# Again calling the function
hello()
# new feature added
# hello() function